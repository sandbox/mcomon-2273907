(function ($) {

/**
 *
 */
Drupal.behaviors.block_link = {
  attach: function (context, settings) {
    $('.block.block-link', context).click(function(e){
      if(e.toElement.localName !== 'a') {
        e.preventDefault();
        var id = $(this).attr('id').replace(/[-]/g, '_');
        var link_settings = eval('Drupal.settings.block_link.' + id);
        window.open(Drupal.settings.basePath + link_settings.block_link_path, link_settings.block_link_target);
      }
    });
  }
};
})(jQuery);
